/*
db.users.insertOne({
    
    "username": "skyMax",
    "password": "doggies2"
    
    })
db.users.insertOne({

    "username": "songuko",
    "password": "supersaiyan"
})


insert multiple documents at once

db.users.insertMany(
    [{
        "username": "PabloEscobar",
        "password": "cartel"
    },
    {
        "username": "elChapo",
        "password": "xxxxxx"
    },
    ]
)

db.products.insertMany([

    {
    "name"       : "book",
    "description": "readable",
    "price"      : "1000"
    },
    {
    "name"       : "shirt",
    "description": "sleveless",
    "price"      : "500"
    },
    {
    "name"       : "LC",
    "description": "V8, 4x4",
    "price"      : "5,000,000"
    }

])

db.users.find()

db.users.find({"username": "PabloEscobar"})

db.cars.insertMany([
    {
        "name": "Vios",
        "brand": "Toyota",
        "type": "Sedan",
        "price": "1500000"
        },
        {
        "name": "Tamaraw FX",
        "brand": "Toyota",
        "type": "auv",
        "price": "750000"
        },
        {
        "name": "City",
        "brand": "Honda",
        "type": "Sedan",
        "price": "1000000"
        }

])


db.cars.find({"type": "Sedan"})
dv.cars.find({"id": ""})

db.cars.findOne({}) //results: finds and returns first item in the collection
db.cars.findOne({"type": "Sedan"}) //results: finds and returns the first item that matches the criteria

db.cars.findOne({"brand": "Toyota"})

db.cars.findOne({"brand": "Honda"})

UPDATE

db.users.updateOne({"username": "PabloEscobar"},{$set:{"username:" : "PeterEscobar"}})
db.users.updateOne({},{$set:{"username": "updatedUsername"}})

db.users.updateOne({"username": "elChapo"},{$set:{"isAdmin": true}})
it the field being updated still does not exist, MongoDB will add that field in the document

db.users.updateMany({},{$set:{"isAdmin": true}}) //update all items in the collection

db.cars.updateMany({"type":"Sedan"},{$set:{"price": "1000000"}})

db.products.deleteOne({}) // deletes first item

db.cars.deleteOne({"brand": "Toyota"})

db.users.deleteMany({"isAdmin": true})
deletes all items that matches the criteria

db.products.deleteMany({})
deletes all documents in the collection
db.cars.deleteMany({})

*/





